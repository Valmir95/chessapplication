package com.example.valmir.chessapplication.move;

import com.example.valmir.chessapplication.Board;
import com.example.valmir.chessapplication.Color;
import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Tile;
import com.example.valmir.chessapplication.pieces.King;
import com.example.valmir.chessapplication.pieces.Pawn;

import java.util.ArrayList;

public class MoveGenerator {
    private Board board;

    public MoveGenerator(Board board){
        this.board = board;
    }

    public ArrayList<Move> getAllLegalMoves(Color color) throws Exception{
        ArrayList<Move> legalMoves = new ArrayList<>();
        ArrayList<Piece> pieces;
        if(color == Color.WHITE){
            pieces = new ArrayList<>(board.getWhitePieces());
        }
        else if(color == Color.BLACK){
            pieces = new ArrayList<>(board.getBlackPieces());
        }
        else {
            throw new Exception("Not supported color");
        }
        for(Piece p : pieces){
            for(Tile t : board.getTiles()){
                if(p.checkValidMove(t.getPosition())){
                    if(!p.checkForCheck(t)){
                        if(p instanceof King){
                            ((King)p).setMoveCastlePosition(-99);
                            ((King)p).setMoveOffsetPosition(-99);
                        }
                        legalMoves.add(new Move(p, p.getTile().getPosition(), t.getPosition()));
                    }
                }
            }
        }

        return legalMoves;
    }

    public ArrayList<Move> getAllLegalMovesPiece(Tile tile) throws Exception{
        Piece p = tile.getPiece();
        ArrayList<Move> legalMoves = new ArrayList<>();
        if(p != null){
            for(Tile t : board.getTiles()){
                if(p.checkValidMove(t.getPosition())){
                    if(p instanceof King){
                        ((King)p).setMoveCastlePosition(-99);
                        ((King)p).setMoveOffsetPosition(-99);
                    }
                    if(!p.checkForCheck(t)){
                        legalMoves.add(new Move(p, tile.getPosition(), t.getPosition()));
                    }
                }
            }
        }
        else{
            throw new Exception("No piece on specified tile");
        }
        return legalMoves;
    }
}
