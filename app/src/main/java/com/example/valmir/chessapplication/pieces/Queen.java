package com.example.valmir.chessapplication.pieces;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.valmir.chessapplication.Color;
import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Position;
import com.example.valmir.chessapplication.R;
import com.example.valmir.chessapplication.Tile;

/**
 * Created by valmir on 20/09/16.
 */

public class Queen implements Piece {
    private Color color;
    private Activity activity;
    private Tile tile;
    public Queen(Color color, Activity activity, Tile tile){
        this.color = color;
        this.activity = activity;
        this.tile = tile;
        this.color.setTurn((this.color == Color.WHITE));

    }
    @Override
    public Piece getType() {
        return this;
    }

    @Override
    public boolean checkValidMove(Position pos) {
        int moveIntervalDown = 8;
        int moveIntervalUp = -8;
        int moveIntervalLeft = -7;
        int moveIntervalRight = 7;
        int[] moveIntervalRanges = this.getTile().getRankRangeFromListPosition(this.getTile().getPosition().getAccurateListPosition());
        int moveOffset = pos.getAccurateListPosition() - tile.getPosition().getAccurateListPosition();


        if(this.getTile().getBoard().getTileByPos(pos).getPiece() != null){
            if(this.getTile().getBoard().getTileByPos(pos).getPiece().getColor() == this.getColor())
                return false;
        }

        //Horizontal and vertical movements
        for(int i = 0; i<moveIntervalDown; i++){
            if((moveOffset == (moveIntervalDown*i)
                    || moveOffset == (moveIntervalUp*i))
                    || (moveOffset == (moveIntervalLeft + i) || moveOffset == (moveIntervalRight - i))
                    && pos.getAccurateListPosition() >= moveIntervalRanges[0]
                    && pos.getAccurateListPosition() <= moveIntervalRanges[1]
                    ){

                if((moveOffset == (moveIntervalLeft + i))
                        && pos.getAccurateListPosition() >= moveIntervalRanges[0]
                        && pos.getAccurateListPosition() <= moveIntervalRanges[1])
                    i = Math.abs(moveOffset);
                else if((moveOffset == (moveIntervalRight - i))
                        && pos.getAccurateListPosition() >= moveIntervalRanges[0]
                        && pos.getAccurateListPosition() <= moveIntervalRanges[1])
                    i = moveOffset;


                System.out.println("The moveoffset is: " + moveOffset);
                System.out.println("The amount of moves are: " + i);

                if( (checkPieceBlockadeVerticalHorizontal(moveOffset, i, pos))){
                    return false;
                }
                return true;

            }
        }

        if(this.getTile().getBoard().getTileByPos(pos).getTileColor() != this.getTile().getTileColor())
            return false;

        //Diagonal movements
        for(int i = 1; i<=8; i++){
            if((moveOffset == (moveIntervalDown * i) + i)
                    || (moveOffset == (moveIntervalDown * i) - i)
                    || (moveOffset == (moveIntervalUp * i) + i )
                    || (moveOffset == (moveIntervalUp * i) - i)) {

                System.out.println("The moveoffset is: " + moveOffset);
                System.out.println("The amount of moves are: " + i);

                if( (checkPieceBlockadeDiagonal(moveOffset, i, pos, false))){
                    return false;
                }


                return true;
            }
        }

        return false;
    }


    public boolean checkPieceBlockadeDiagonal(int moveOffset, int amountOfMoves, Position toTile, boolean isPositive){
        int downWardOffset;
        int upWardOffset;
        int listPos;
        if(moveOffset % 9 == 0){
            downWardOffset = 9;
            upWardOffset = -9;
        }

        else{
            downWardOffset = 7;
            upWardOffset = -7;
        }


        for(int i = 1; i<amountOfMoves;i++){

            if(moveOffset > 0)  listPos = toTile.getAccurateListPosition() - (moveOffset-(i*downWardOffset));
            else listPos = toTile.getAccurateListPosition() - (moveOffset-(i*upWardOffset));

            Tile listPostTile = this.getTile().getBoard().getTiles().get(listPos);
            System.out.println(this.getColor());

            if(listPostTile.getPiece() != null && !(listPostTile.getPiece() == this)) {

                System.out.println("This piece is in the way: " + this.getTile().getBoard().getTiles().get(listPos).getPiece().getType()
                        + " at pos: " + this.getTile().getBoard().getTiles().get(listPos).getPosition().getAccurateListPosition());
                return true;
            }


        }
        return false;
    }


    public boolean checkPieceBlockadeVerticalHorizontal(int moveOffset, int amountOfMoves, Position toTile){
        int downWardOffset;
        int upWardOffset;
        int listPos;
        if(moveOffset % 8 == 0){
            downWardOffset = 8;
            upWardOffset = -8;
        }
        else{
            downWardOffset = 1;
            upWardOffset = -1;
        }
        for(int i = 1; i<amountOfMoves;i++){
            if(moveOffset > 0)  listPos = toTile.getAccurateListPosition() - (moveOffset-(i*downWardOffset));
            else listPos = toTile.getAccurateListPosition() - (moveOffset-(i*upWardOffset));

            Tile listPostTile = this.getTile().getBoard().getTiles().get(listPos);
            System.out.println(this.getColor());

            if(listPostTile.getPiece() != null && !(listPostTile.getPiece() == this)) {

                System.out.println("This piece is in the way: " + this.getTile().getBoard().getTiles().get(listPos).getPiece().getType()
                        + " at pos: " + this.getTile().getBoard().getTiles().get(listPos).getPosition().getAccurateListPosition());
                return true;
            }
        }
        return false;
    }


    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public String getFENNotation() {
        if(this.getColor() == Color.WHITE){
            return "Q";
        }
        return "q";
    }

    @Override
    public boolean movePiece(Tile toTile) {

        if(checkValidMove(toTile.getPosition())){
            this.getTile().getBoard().incrementHalfMoves();

            if(toTile.getPiece() != null){
                this.getTile().getBoard().resetHalfMoves();
            }

           if(checkForCheck(toTile)){
               return false;
           }

            toTile.setPiece(this);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            return true;
        }
        return false;


    }

    public boolean checkForCheck(Tile toTile){
        Tile previousTile = this.getTile();
        Piece previousPiece = toTile.getPiece();

        this.setTile(toTile);
        this.getTile().setPiece(this);
        previousTile.setPiece(null);
        System.out.println("check simulation move: " + this.getTile().getPosition().getX() + this.getTile().getPosition().getY());
        if(this.getTile().getBoard().isChecked(this.getColor(), this.getTile().getBoard().getKing(this.getColor()).getTile().getPosition())){
            System.out.println("Cannot move because CHECKED!");
            previousTile.setPiece(this);
            this.setTile(previousTile);
            toTile.setPiece(previousPiece);
            return true;
        }
        previousTile.setPiece(this);
        this.setTile(previousTile);
        toTile.setPiece(previousPiece);
        return false;
    }


    @Override
    public boolean capturePiece() {
        return false;
    }

    @Override
    public boolean thisPieceCaptured() {
        return false;
    }


    @Override
    public Tile getTile() {
        return this.tile;
    }


    @Override
    public Bitmap getBitMap() {
        if(color == Color.WHITE)
            return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.queen00),
                    getTile().getBoard().getGridWidth(),
                    getTile().getBoard().getGridHeight());

        return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.queen11),
                getTile().getBoard().getGridWidth(),
                getTile().getBoard().getGridHeight());
    }

    @Override
    public void setTile(Tile tile) {
        this.tile = tile;
    }
}
