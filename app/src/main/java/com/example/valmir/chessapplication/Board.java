package com.example.valmir.chessapplication;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.widget.GridLayout;
import android.widget.ImageButton;


import com.example.valmir.chessapplication.move.Move;
import com.example.valmir.chessapplication.move.MoveGenerator;
import com.example.valmir.chessapplication.pieces.Bishop;
import com.example.valmir.chessapplication.pieces.King;
import com.example.valmir.chessapplication.pieces.Knight;
import com.example.valmir.chessapplication.pieces.Pawn;
import com.example.valmir.chessapplication.pieces.Queen;
import com.example.valmir.chessapplication.pieces.Rook;
import com.example.valmir.chessapplication.stockfish.Stockfish;

import java.util.ArrayList;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;

/**
 * Created by valmir on 20/09/16.
 */

public class Board {
    private final int DIMENSION = 8;


    private ArrayList<Tile> tiles;
    private GridLayout gridLayout;
    private Activity activity;
    private final static String[] xPositions = {"A", "B", "C", "D", "E", "F", "G", "H"};
    private final static int[] yPositions = {8, 7, 6, 5, 4, 3, 2, 1};
    private DisplayMetrics metrics;
    private int gridWidth;
    private int gridHeight;
    private ArrayList<Piece> whitePieces;
    private ArrayList<Piece> blackPieces;
    private Color white = Color.WHITE;
    private Color black = Color.BLACK;
    private CountdownView whiteTimer;
    private CountdownView blackTimer;
    private int fullMoves;
    private int halfMoves;
    private List<String> FENList;

    private long previousWhiteTime;
    private long previousBlackTime;


    private long startWhiteTime;
    private long startBlackTime;
    private boolean gameOver;
    private MoveGenerator moveGenerator;
    private Stockfish stockfish;
    private ArrayList<Tile>markedTiles;



    public Board(GridLayout gridLayout, Activity activity, DisplayMetrics metrics, CountdownView whiteTimer, CountdownView blackTimer){
        this.gridLayout = gridLayout;
        this.activity = activity;
        this.tiles = new ArrayList<>();
        this.whitePieces = new ArrayList<>();
        this.blackPieces = new ArrayList<>();
        this.FENList = new ArrayList<>();
        this.metrics = metrics;
        this.gridHeight = metrics.widthPixels/8;
        this.gridWidth = metrics.widthPixels/8;
        this.whiteTimer = whiteTimer;
        this.blackTimer = blackTimer;
        this.previousWhiteTime = 0;
        this.previousBlackTime = 0;
        this.fullMoves = 1;
        this.halfMoves = 0;
        this.gameOver = false;
        this.moveGenerator = new MoveGenerator(this);
        this.stockfish = new Stockfish();
        this.markedTiles = new ArrayList<>();
    }

    public ArrayList<Tile> getTiles(){
        return this.tiles;
    }


    protected void drawBoard(long whiteTime, long blackTime){
        this.startWhiteTime = whiteTime;
        this.startBlackTime = blackTime;

        //Empty previous board if any

        gridLayout.setColumnCount(DIMENSION);
        gridLayout.setRowCount(DIMENSION);
        String backGroundColor;

        GridLayout.LayoutParams params;

        for (int rowCounter = 0; rowCounter < DIMENSION; rowCounter++){
            for (int columnCounter = 0; columnCounter < DIMENSION; columnCounter++) {
                ImageButton b = new ImageButton(activity);
                params = new GridLayout.LayoutParams();
                params.width = gridWidth;
                params.height = gridHeight;
                b.setLayoutParams(params);
                Color tileColor = black;
                if(rowCounter % 2 == 0){
                    if(columnCounter % 2 == 0){
                        backGroundColor = "#D7CCC8";
                        tileColor = white;

                    }
                    else{
                        backGroundColor = "#795548";
                        tileColor = black;//////

                    }
                }
                else {
                    if(columnCounter % 2 == 1){
                        backGroundColor = "#D7CCC8";
                        tileColor = white;

                    }
                    else{
                        backGroundColor = "#795548";
                        tileColor = black;///////
                    }
                }
                b.setBackgroundColor(android.graphics.Color.parseColor(backGroundColor));

                tiles.add(new Tile(b, new Position(xPositions[columnCounter], yPositions[rowCounter]), null, backGroundColor, this, tileColor));
                gridLayout.addView(b);
            }
        }
        setOnClickListeners();
        createAndPlacePieces();
        debugPieces();

        whiteTimer.start(whiteTime);
        blackTimer.updateShow(blackTime);
        previousBlackTime = blackTime;
        //blackTimer.pause();
        ArrayList<Move>legalMoves;
        ArrayList<Move>legalMovesPiece;

        try{
            //legalMoves = this.moveGenerator.getAllLegalMoves(Color.WHITE);
            //legalMovesPiece = this.moveGenerator.getAllLegalMovesPiece(this.getWhitePieces().get(0).getTile());
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        System.out.println(generateFENStringCurrentPositions());

    }

    public void restartBoard(){
        this.removeFocuses();
        this.emptyTiles(this.getTiles());
        this.createAndPlacePieces();
        this.setGameOver(false);

        whiteTimer.start(this.getStartWhiteTime());
        blackTimer.pause();
        blackTimer.updateShow(this.getStartBlackTime());
        previousBlackTime = this.getStartBlackTime();
        //this.restartTimers();
    }

    /*private void restartTimers(){
        TextView t = (TextView)this.getActivity().findViewById(R.id.blackTimer);

        this.getWhiteTimer().cancel();
        this.getBlackTimer().cancel();
        this.getWhiteTimer().start();
        this.getBlackTimer().start();
        t.setText("Black time: "+ (this.getBlackTimer().pause() - 1)/1000);

    }*/

    private void emptyTiles(ArrayList<Tile> tiles){
        for(Tile t : tiles){
            t.getButton().setImageBitmap(null);
            t.setPiece(null);
        }
    }

    private void setOnClickListeners(){
        for(Tile t : this.getTiles()){
            t.getButton().setOnClickListener(t.listener);
        }
    }

    public boolean checkAnyTilesFocused(){
        for(Tile tile : this.getTiles()){
            if(tile.isFocused())
                return true;
        }
        return false;
    }

    public Tile getFocusedTile(){
        for(Tile tile : this.getTiles()){
            if(tile.isFocused()){
                return tile;
            }
        }
        return null;
    }


    public Tile getTileByPos(Position pos){
        for(Tile t : this.getTiles()){
            if(t.getPosition().equals(pos)){
                System.out.println("Did find tile by that position: "+ pos.toString());
                return t;

            }
        }
        return null;
    }



    public Tile getTileByAn(AlgebraicNotation an){
        for (Tile t : this.tiles){
            AlgebraicNotation algebraicNotation = t.getPosition().getAlgebraicNotation();
            if(algebraicNotation.equals(an)){
                return t;
            }
        }
        return null;
    }

    public void move(Tile from, Tile to){
        Piece p = from.getPiece();
        if(p != null){
            boolean movedPiece = p.movePiece(to);
            if(movedPiece){
                from.setPiece(null);
                from.getButton().setImageBitmap(null);
                to.setPiece(p);
                p.setTile(to);

                this.removeEnPassantePositions(p.getColor());
                this.setTurnsFalse(p.getColor());
                this.removeFocuses();
                from.getButton().setBackgroundColor(android.graphics.Color.CYAN);
                to.getButton().setBackgroundColor(android.graphics.Color.CYAN);
                this.addMarkedTile(from);
                this.addMarkedTile(to);
                this.addFENToList(this.generateFENStringCurrentPositions());
            }
        }
    }


    public Stockfish getStockfish(){
        return this.stockfish;
    }



    public void computerMove(){
        int listSize = this.getFENList().size();
        String latestFEN = this.getFENList().get(listSize-1);
        String[] bestMoveAndVal;
        try{
            bestMoveAndVal = this.stockfish.getBestMoveAndEval(latestFEN, 0.2);
        }catch (Exception e){
            e.printStackTrace();
            return;
        }


        String fromAnPos = bestMoveAndVal[0].substring(0,2);
        String toAnPos = bestMoveAndVal[0].substring(bestMoveAndVal[0].length()-2);
        Tile from = this.getTileByAn(new AlgebraicNotation(fromAnPos.substring(0,1), Integer.parseInt(fromAnPos.substring(fromAnPos.length()-1))));
        Tile to = this.getTileByAn(new AlgebraicNotation(toAnPos.substring(0,1), Integer.parseInt(toAnPos.substring(toAnPos.length()-1))));

        this.move(from, to);
    }



    public Activity getActivity(){
        return this.activity;
    }

    public int getHalfMoves(){
        return this.halfMoves;
    }
    public void incrementHalfMoves(){
        this.halfMoves++;
    }

    public void resetHalfMoves(){
        this.halfMoves = 0;
    }
    public void resetFullMoves(){
        this.fullMoves = 1;
    }

    public int getFullMoves(){
        return this.fullMoves;
    }
    public void incrementFullMoves(){
        this.fullMoves++;
    }

    public long getStartWhiteTime(){
        return this.startWhiteTime;
    }

    public long getStartBlackTime(){
        return this.startBlackTime;
    }

    public CountdownView getWhiteTimer(){
        return this.whiteTimer;
    }

    public CountdownView getBlackTimer(){
        return this.blackTimer;
    }

    public void setWhiteTimer(CountdownView whiteTimer){
        this.whiteTimer = whiteTimer;
    }

    public void setBlackTimer(CountdownView blackTimer){
        this.blackTimer = blackTimer;
    }

    public List<String> getFENList(){
        return this.FENList;
    }

    public void addFENToList(String FEN){
        this.FENList.add(FEN);
    }

    public String generateFENStringCurrentPositions(){
        String FEN = "";
        FEN += this.getFenBoardPositions();
        FEN += this.getFENTurn();
        FEN += this.getFENCastlingPossibilities();
        FEN += this.getFenPassante();
        FEN += this.getHalfMoves();
        FEN += " ";
        FEN += this.getFullMoves();

        return FEN;
    }

    public String getFenPassante(){
        for(Tile t : this.getTiles()){
            if(t.getPiece() instanceof Pawn){
                Pawn tempPawn = (Pawn)t.getPiece();
                if(tempPawn.getEnPassantePosition() != null){
                    return " " + tempPawn.getEnPassantePosition().getX().toLowerCase() + tempPawn.getEnPassantePosition().getY() + " ";
                }
            }
        }
        return " - ";
    }

    public String getFENTurn(){
        if(black.getTurn()){
            return " b ";
        }
        else if(white.getTurn()){
            return " w ";
        }
        return null;
    }

    public String getFENCastlingPossibilities(){
        String FENCastling = "";
        final int BlackCastlingPositions[] = {0, 4, 7};
        final int WhiteCastlingPositions[] = {56, 60, 63};
        Piece blackQueenRook = this.getTiles().get(BlackCastlingPositions[0]).getPiece();
        Piece blackKingRook = this.getTiles().get(BlackCastlingPositions[2]).getPiece();
        Piece blackKing = this.getTiles().get(BlackCastlingPositions[1]).getPiece();

        Piece whiteQueenRook = this.getTiles().get(WhiteCastlingPositions[0]).getPiece();
        Piece whiteKingRook = this.getTiles().get(WhiteCastlingPositions[2]).getPiece();
        Piece whiteKing = this.getTiles().get(WhiteCastlingPositions[1]).getPiece();

        if((whiteKingRook != null
                && whiteKingRook instanceof Rook)
                &&  !((Rook) whiteKingRook).getHasMadeFirstMove() && whiteKing != null
                && whiteKing instanceof King && !((King)whiteKing).getHasMadeFirstMove()){

            FENCastling += "K";
        }

        if((whiteQueenRook != null
                && whiteQueenRook instanceof Rook)
                &&  !((Rook) whiteQueenRook).getHasMadeFirstMove() && whiteKing != null
                && whiteKing instanceof King && !((King)whiteKing).getHasMadeFirstMove()){

            FENCastling += "Q";
        }

        if((blackKingRook != null
                && blackKingRook instanceof Rook)
                &&  !((Rook) blackKingRook).getHasMadeFirstMove() && blackKing != null
                && blackKing instanceof King && !((King)blackKing).getHasMadeFirstMove()){

            FENCastling += "k";
        }

        if((blackQueenRook != null
                && blackQueenRook instanceof Rook)
                &&  !((Rook) blackQueenRook).getHasMadeFirstMove() && blackKing != null
                && blackKing instanceof King && !((King)blackKing).getHasMadeFirstMove()){

            FENCastling += "q";
        }
        if(FENCastling.isEmpty()){
            FENCastling = "-";
        }

        return FENCastling;
    }



    public String getFenBoardPositions(){
        String FEN = "";
        int tileCounter = 0;
        int noPiecesOnTile = 0;
        int tilesInRow = 0;
        for(int i = 0; i<8;i++){
            for(int j = 0; j<8;j++){
                tilesInRow++;

                if(this.getTiles().get(tileCounter).getPiece() != null){
                    if (noPiecesOnTile > 0) {
                        FEN += Integer.toString(noPiecesOnTile);
                    }
                    FEN += this.getTiles().get(tileCounter).getPiece().getFENNotation();
                    noPiecesOnTile = 0;
                }
                else{
                    noPiecesOnTile++;
                    if(tilesInRow == 8){
                        FEN += Integer.toString(noPiecesOnTile);
                        noPiecesOnTile = 0;
                    }
                    if (noPiecesOnTile == 8) {
                        FEN += Integer.toString(noPiecesOnTile);
                        noPiecesOnTile = 0;
                    }
                }
                tileCounter++;
            }
            if(noPiecesOnTile>0){
                noPiecesOnTile = 0;
            }
            FEN += "/";
            tilesInRow = 0;

        }
        return FEN.substring(0,FEN.length()-1);
    }


    public void removeFocuses(){
        for(Tile tile : this.getTiles())
            tile.focusButton(false);
    }

    public void setTurnsFalse(Color color){
        if(color == white){
            white.setTurn(false);
            black.setTurn(true);
            whiteTimer.pause();
            previousWhiteTime = whiteTimer.getRemainTime();
            blackTimer.start(previousBlackTime);
        }
        else if (color == black){
            white.setTurn(true);
            black.setTurn(false);
            whiteTimer.start(previousWhiteTime);
            blackTimer.pause();
            previousBlackTime = blackTimer.getRemainTime();
        }
    }

    public void removeEnPassantePositions(Color color){
        if(color == white){
            for(Piece p : this.getBlackPieces()){
                if(p.getType() instanceof Pawn){
                    ((Pawn)p).setEnPassantePosition(null);
                }
            }
        }
        else if(color == black){
            for(Piece p : this.getWhitePieces()){
                if(p.getType() instanceof Pawn){
                    ((Pawn)p).setEnPassantePosition(null);
                }
            }
        }
    }

    public ArrayList<Piece> getWhitePieces(){
        whitePieces.clear();
        for(Tile t: this.getTiles()){
            if(t.getPiece() != null){
                if(t.getPiece().getColor() == white)
                    whitePieces.add(t.getPiece());
            }

        }

        return whitePieces;
    }

    public ArrayList<Piece> getBlackPieces(){
        blackPieces.clear();
        for(Tile t: this.getTiles()){
            if(t.getPiece() != null){
                if(t.getPiece().getColor() == black)
                    blackPieces.add(t.getPiece());
            }
        }

        return blackPieces;
    }

    public King getKing(Color color){
        if(color == white){
            for(Piece p : getWhitePieces()){
                if(p instanceof King){
                    return (King)p;
                }
            }
        }

        else if(color == black){
            for(Piece p : getBlackPieces()){
                if(p instanceof King){
                    return (King)p;
                }
            }
        }
        return null;
    }

    public MoveGenerator getMoveGenerator(){
        return this.moveGenerator;
    }

    public boolean isGameOver(){

        if(this.getHalfMoves() > 49){
            this.setGameOver(true);
            return true;
        }
        try{
            if(Color.WHITE.getTurn()){
                if(this.getMoveGenerator().getAllLegalMoves(Color.WHITE).size() == 0){
                    this.setGameOver(true);
                    return true;
                }
            }
            else{
                if(this.getMoveGenerator().getAllLegalMoves(Color.BLACK).size() == 0){
                    this.setGameOver(true);
                    return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public boolean isCheckMated(){

        if(Color.WHITE.getTurn()){
            try{
                ArrayList<Move> legalMoves = this.getMoveGenerator().getAllLegalMoves(Color.WHITE);
                if(legalMoves.size() == 0 && this.isChecked(Color.WHITE, this.getKing(Color.WHITE).getTile().getPosition())){
                    this.setGameOver(true);
                    return true;
                }
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }
        else{
            try{
                ArrayList<Move> legalMoves = this.getMoveGenerator().getAllLegalMoves(Color.BLACK);
                if(legalMoves.size() == 0 && this.isChecked(Color.BLACK, this.getKing(Color.BLACK).getTile().getPosition())){
                    this.setGameOver(true);
                    return true;
                }
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public boolean getGameOver(){
        return this.gameOver;
    }

    public void setGameOver(boolean gameOver){
        this.gameOver = gameOver;
    }

    public boolean isChecked(Color pieceColor, Position kingPosition){
        System.out.println(this.getTiles().get(42).getPiece());
        System.out.println("Size of white piece list: " + this.getWhitePieces().size() + " Size of black piece list: " + this.getBlackPieces().size());
        if(pieceColor == white){
            for(Piece p : this.getBlackPieces()){
                if(p.checkValidMove(kingPosition)){
                    System.out.println("Checked by: " + p.getType() + " " + p.getTile().getPosition().getX() + p.getTile().getPosition().getY());
                    pieceColor.setChecked(true);
                    return true;
                }
            }
            pieceColor.setChecked(false);
        }
        else if(pieceColor == black){
            for(Piece p : this.getWhitePieces()){
                if(p.checkValidMove(kingPosition)){
                    System.out.println("Checked by: " + p.getType());
                    pieceColor.setChecked(true);
                    return true;
                }
            }
            pieceColor.setChecked(false);
        }

        return false;
    }

    public ArrayList<Tile>getMarkedTiles(){
        return this.markedTiles;
    }

    public void addMarkedTile(Tile t){
        this.markedTiles.add(t);
    }


    public boolean isKingUnderAttack(King king){

        ArrayList<Piece> pieces;
        if(king.getColor() == Color.WHITE){
            pieces = this.getBlackPieces();
        }
        else {
            pieces = this.getWhitePieces();
        }
        for(Piece p : pieces){
            if(p.checkValidMove(king.getTile().getPosition())){
                return true;
            }
        }
        return false;
    }

    public boolean isPathClearForCastling(com.example.valmir.chessapplication.Color pieceColor, int [] castlingPath){
        if(pieceColor == black){
            for(Piece p : this.getWhitePieces()){
                for(int i = 0; i<castlingPath.length;i++){
                    if(p.checkValidMove(this.getTiles().get(castlingPath[i]).getPosition())){
                        return false;
                    }
                }
            }
        }
        else if(pieceColor == white){
            for(Piece p : this.getBlackPieces()){
                for(int i = 0; i<castlingPath.length;i++){
                    if(p.checkValidMove(this.getTiles().get(castlingPath[i]).getPosition())){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void debugPieces(){
        System.out.println("List of white pieces: ");
        for(Tile t : this.getTiles()){
            if(t.getPiece() != null){
                if(t.getPiece().getColor() == white){
                    System.out.println(t.getPiece().getType());
                }
            }

        }

        System.out.println("List of black pieces: ");
        for(Tile t : this.getTiles()){
            if(t.getPiece() != null){
                if(t.getPiece().getColor() == black){
                    System.out.println(t.getPiece().getType());
                }
            }

        }

    }


    private void createAndPlacePieces(){
        placePawns();
        placeRooks();
        placeKnights();
        placeBishops();
        placeKingAndQueen();
        //this.placeKingQueenEnding();

    }

    private void placeKingQueenEnding(){
        this.placeKingAndQueen();
    }

    private void placePawns(){
        int pawnPos = 48;
        Color color = white;

        for(int i = 0; i<2;i++){
            if(i>0){
                pawnPos = 8;
                color = black;
            }
            for(int j = 0; j < 8; j++){
                Tile t = this.getTiles().get(pawnPos+j);
                t.setPiece(new Pawn(color, activity,t));
                t.getButton().setImageBitmap(t.getPiece().getBitMap());
            }
        }
    }

    private void placeRooks(){
        int[] rookPosBlack = {63,7};
        int[] rookPosWhite = {56,0};
        Tile t;

        for(int i = 0;i<rookPosWhite.length; i++){
            t = this.getTiles().get(rookPosWhite[i]);
            t.setPiece(new Rook(Color.values()[i], activity, t));
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }

        for(int i = 0;i<rookPosBlack.length; i++){
            t = this.getTiles().get(rookPosBlack[i]);
            t.setPiece(new Rook(Color.values()[i], activity, t));
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }

    }

    private void placeKnights(){
        int[] knightPosBlack = {62,6};
        int[] knightPosWhite = {57,1};



        Tile t;

        for(int i = 0;i<knightPosWhite.length; i++){
            t = this.getTiles().get(knightPosWhite[i]);
            t.setPiece(new Knight(Color.values()[i], activity, t));
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }

        for(int i = 0;i<knightPosBlack.length; i++){
            t = this.getTiles().get(knightPosBlack[i]);
            t.setPiece(new Knight(Color.values()[i], activity, t));
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }
    }

    private void placeBishops(){
        int[] bishopPosBlack = {61,5};
        int[] bishopPosWhite = {58,2};
        Tile t;

        for(int i = 0;i<bishopPosWhite.length; i++){
            t = this.getTiles().get(bishopPosWhite[i]);
            t.setPiece(new Bishop(Color.values()[i], activity, t));
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }

        for(int i = 0;i<bishopPosBlack.length; i++){
            t = this.getTiles().get(bishopPosBlack[i]);
            t.setPiece(new Bishop(Color.values()[i], activity, t));
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }
    }

    private void placeKingAndQueen(){
        int[] kingPos = {60,4};
        int[] queenPos= {59,3};

        King[] kings = {new King(white, activity, null), new King(black,activity, null)};
        Queen[] queens = {new Queen(white,activity, null), new Queen(black,activity, null)};

        Tile t;
        for(int i = 0; i<kingPos.length;i++){
            t = this.getTiles().get(kingPos[i]);
            kings[i].setTile(t);
            t.setPiece(kings[i]);
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }
        for(int i = 0; i<queenPos.length;i++){
            t = this.getTiles().get(queenPos[i]);
            queens[i].setTile(t);
            t.setPiece(queens[i]);
            t.getButton().setImageBitmap(t.getPiece().getBitMap());
        }



    }

    public int getGridWidth(){
        return this.gridWidth;
    }

    public int getGridHeight(){
        return this.gridHeight;
    }

    public Bitmap scaleBitmap(Bitmap b, final int w, final int h){
        return Bitmap.createScaledBitmap(b, w,h,false);
    }


}
