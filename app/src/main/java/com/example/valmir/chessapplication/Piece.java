package com.example.valmir.chessapplication;

import android.graphics.Bitmap;

/**
 * Created by valmir on 20/09/16.
 */

public interface Piece {


    public Piece getType();

    public boolean checkValidMove(Position pos);

    public Color getColor();

    public boolean movePiece(Tile tile);

    public boolean capturePiece();

    public boolean thisPieceCaptured();

    public String getFENNotation();

    public boolean checkForCheck(Tile toTile);



    public Tile getTile();

    public Bitmap getBitMap();

    public void setTile(Tile tile);


}
