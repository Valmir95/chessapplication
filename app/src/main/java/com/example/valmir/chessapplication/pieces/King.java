package com.example.valmir.chessapplication.pieces;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.valmir.chessapplication.Color;
import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Position;
import com.example.valmir.chessapplication.R;
import com.example.valmir.chessapplication.Tile;

import java.util.EnumSet;

public class King implements Piece {
    private Color color;
    private Activity activity;
    private Tile tile;
    private boolean hasMadeFirstMove;

    private static final int[] ROOK_WHITE_LIST_POSITIONS = {56,63};
    private static final int[] ROOK_BLACK_LIST_POSITIONS = {0,7};

    private static final int[] KING_WHITE_RIGHT_PATH = {61,62};
    private static final int[] KING_WHITE_LEFT_PATH = {57,58,59};
    private static final int[] KING_WHITE_LEFT_PATH_KING_MOVE = {58,59};

    private static final int[] KING_BLACK_RIGHT_PATH = {5,6};
    private static final int[] KING_BLACK_LEFT_PATH = {1,2,3};
    private static final int[] KING_BLACK_LEFT_PATH_KING_MOVE = {2,3};


    private static final int KING_CASTLING_RIGHT_OFFSET = 2;
    private static final int KING_CASTLING_LEFT_OFFSET = -2;
    //If a valid king move has been interpreted as a way of castling, these will be set in the checkValidMove method, rather than be executed there.
    private int moveCastlePosition;
    private int moveOffsetPosition;



    public King(Color color, Activity activity, Tile tile){

        this.color = color;
        this.activity = activity;
        this.tile = tile;
        this.color.setTurn((this.color == Color.WHITE));
        this.hasMadeFirstMove = false;
        this.moveCastlePosition = -99;
        this.moveOffsetPosition = -99;

    }
    @Override
    public Piece getType() {
        return this;
    }

    //TODO: BUG: When offset is -1 or 1, king can move from all the way right, to the first tile to the left in the next rank.
    @Override
    public boolean checkValidMove(Position pos) {
        int [] moveOffsetMoves = {-9, -8, -7, -1, 1, 7, 8, 9};
        int moveOffset = pos.getAccurateListPosition() - tile.getPosition().getAccurateListPosition();
        int[] moveIntervalRanges = this.getTile().getRankRangeFromListPosition(this.getTile().getPosition().getAccurateListPosition());

        if( (moveOffset == moveOffsetMoves[3] || moveOffset == moveOffsetMoves[4]) && this.getTile().getTileColor() == this.getTile().getBoard().getTileByPos(pos).getTileColor() ){
            return false;
        }
        if( (moveOffset == moveOffsetMoves[0] || moveOffset == moveOffsetMoves[2] || moveOffset == moveOffsetMoves[5] || moveOffset == moveOffsetMoves[7]) && this.getTile().getTileColor() != this.getTile().getBoard().getTileByPos(pos).getTileColor() ){
            return false;
        }

        if(this.getTile().getBoard().getTileByPos(pos).getPiece() != null){
            if(this.getTile().getBoard().getTileByPos(pos).getPiece().getColor() == this.getColor())
                return false;
        }
        //King sided castling for both colors
        if(!this.getHasMadeFirstMove() && !checkPieceBlockade(moveOffset, Math.abs(moveOffset), pos)){
            if(moveOffset == KING_CASTLING_RIGHT_OFFSET && this.getColor() == Color.WHITE){
                this.moveCastlePosition = ROOK_WHITE_LIST_POSITIONS[1];
                this.moveOffsetPosition = Math.abs(moveOffset);

                return true;
            }
            else if(moveOffset == KING_CASTLING_RIGHT_OFFSET && this.getColor() == Color.BLACK){
                this.moveCastlePosition = ROOK_BLACK_LIST_POSITIONS[1];
                this.moveOffsetPosition = Math.abs(moveOffset);
                return true;
            }

            else if(moveOffset == KING_CASTLING_LEFT_OFFSET && this.getColor() == Color.WHITE){
                this.moveCastlePosition = ROOK_WHITE_LIST_POSITIONS[0];
                this.moveOffsetPosition = moveOffset - 1;
                return true;
            }

            else if(moveOffset == KING_CASTLING_LEFT_OFFSET && this.getColor() == Color.BLACK){
                this.moveCastlePosition = ROOK_BLACK_LIST_POSITIONS[0];
                this.moveOffsetPosition = moveOffset - 1;
                return true;
            }
        }

        for(int i = 0; i<moveOffsetMoves.length; i++){
            if((moveOffset == -9 || moveOffset == -7 || moveOffset == 7 || moveOffset == 9)
                    &&
                    (pos.getAccurateListPosition() >= moveIntervalRanges[0]
                    && pos.getAccurateListPosition() <= moveIntervalRanges[1])
                    || (moveOffset == 1 || moveOffset == -1) && (pos.getAccurateListPosition() > moveIntervalRanges[0]
                    && pos.getAccurateListPosition() > moveIntervalRanges[1])){
                return false;
            }
            if(moveOffset == moveOffsetMoves[i]){
                return true;
            }
        }
        return false;
    }

    private void moveRookCastle(int rookListPost, int moveOffset){
        Tile toTile = this.getTile().getBoard().getTiles().get(rookListPost-moveOffset);
        Tile fromTile = this.getTile().getBoard().getTiles().get(rookListPost);

        toTile.setPiece(this.getTile().getBoard().getTiles().get(rookListPost).getPiece());
        toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
        toTile.getPiece().setTile(toTile);
        Rook r = (Rook) toTile.getPiece();
        r.setHasMadeFirstMove(true);

        fromTile.setPiece(null);
        fromTile.getButton().setImageBitmap(null);

    }

    private boolean checkPieceBlockade(int moveOffset, int amountOfMoves, Position toTile){
        //King sided castling
        if(moveOffset == KING_CASTLING_RIGHT_OFFSET){
            //King sided white
            if(this.getTile().getBoard().getTiles().get(ROOK_WHITE_LIST_POSITIONS[1]).getPiece() != null){
                if(this.getColor() == Color.WHITE && (this.getTile().getBoard().getTiles().get(KING_WHITE_RIGHT_PATH[0]).getPiece() == null
                        && this.getTile().getBoard().getTiles().get(KING_WHITE_RIGHT_PATH[1]).getPiece() == null)
                        && (this.getTile().getBoard().getTiles().get(ROOK_WHITE_LIST_POSITIONS[1]).getPiece() instanceof Rook)
                        &&  !(((Rook) this.getTile().getBoard().getTiles().get(ROOK_WHITE_LIST_POSITIONS[1]).getPiece()).getHasMadeFirstMove())
                        && this.getTile().getBoard().isPathClearForCastling(this.getColor(), KING_WHITE_RIGHT_PATH)){
                    return false;
                }
            }
            //King sided black
            if(this.getTile().getBoard().getTiles().get(ROOK_BLACK_LIST_POSITIONS[1]).getPiece() != null){
                if((this.getColor() == Color.BLACK && (this.getTile().getBoard().getTiles().get(KING_BLACK_RIGHT_PATH[0]).getPiece() == null
                        && this.getTile().getBoard().getTiles().get(KING_BLACK_RIGHT_PATH[1]).getPiece() == null)
                        && (this.getTile().getBoard().getTiles().get(ROOK_BLACK_LIST_POSITIONS[1]).getPiece() instanceof Rook)
                        &&  !(((Rook) this.getTile().getBoard().getTiles().get(ROOK_BLACK_LIST_POSITIONS[1]).getPiece()).getHasMadeFirstMove()))
                        && this.getTile().getBoard().isPathClearForCastling(this.getColor(), KING_BLACK_RIGHT_PATH)){
                    return false;
                }
            }
        }
        //Queen sided castling
        if(moveOffset == KING_CASTLING_LEFT_OFFSET){
            //Queen sided white
            if(this.getTile().getBoard().getTiles().get(ROOK_WHITE_LIST_POSITIONS[0]).getPiece() != null){
                if(this.getColor() == Color.WHITE && (this.getTile().getBoard().getTiles().get(KING_WHITE_LEFT_PATH[0]).getPiece() == null
                && this.getTile().getBoard().getTiles().get(KING_WHITE_LEFT_PATH[1]).getPiece() == null
                && this.getTile().getBoard().getTiles().get(KING_WHITE_LEFT_PATH[2]).getPiece() == null)
                    && (this.getTile().getBoard().getTiles().get(ROOK_WHITE_LIST_POSITIONS[0]).getPiece() instanceof Rook)
                    && !(((Rook) this.getTile().getBoard().getTiles().get(ROOK_WHITE_LIST_POSITIONS[0]).getPiece()).getHasMadeFirstMove())
                        && this.getTile().getBoard().isPathClearForCastling(this.getColor(), KING_WHITE_LEFT_PATH_KING_MOVE)){

                    return false;
                }
            }

            if(this.getTile().getBoard().getTiles().get(ROOK_BLACK_LIST_POSITIONS[0]).getPiece() != null){
                //Queen sided black
                if(this.getColor() == Color.BLACK && (this.getTile().getBoard().getTiles().get(KING_BLACK_LEFT_PATH[0]).getPiece() == null
                        && this.getTile().getBoard().getTiles().get(KING_BLACK_LEFT_PATH[1]).getPiece() == null
                        && this.getTile().getBoard().getTiles().get(KING_BLACK_LEFT_PATH[2]).getPiece() == null)
                        && (this.getTile().getBoard().getTiles().get(ROOK_BLACK_LIST_POSITIONS[0]).getPiece() instanceof Rook)
                        && !(((Rook) this.getTile().getBoard().getTiles().get(ROOK_BLACK_LIST_POSITIONS[0]).getPiece()).getHasMadeFirstMove())
                        && this.getTile().getBoard().isPathClearForCastling(this.getColor(), KING_BLACK_LEFT_PATH_KING_MOVE)){


                    return false;
                }
            }
        }


        return true;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public boolean movePiece(Tile toTile) {

        if(checkValidMove(toTile.getPosition())){
            this.getTile().getBoard().incrementHalfMoves();

            if(toTile.getPiece() != null){
                this.getTile().getBoard().resetHalfMoves();
            }
            if(this.moveCastlePosition != -99 && this.moveOffsetPosition != -99){
                this.moveRookCastle(this.moveCastlePosition, this.moveOffsetPosition);
            }
            if (checkForCheck(toTile)){
                return false;
            }



            toTile.setPiece(this);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            this.setHasMadeFirstMove(true);
            return true;
        }
        return false;

    }

    @Override
    public String getFENNotation() {
        if(this.getColor() == Color.WHITE){
            return "K";
        }
        return "k";
    }

    public boolean checkForCheck(Tile toTile){
        Tile previousTile = this.getTile();
        Piece previousPiece = toTile.getPiece();

        this.setTile(toTile);
        this.getTile().setPiece(this);
        previousTile.setPiece(null);
        System.out.println("check simulation move: " + this.getTile().getPosition().getX() + this.getTile().getPosition().getY());
        if(this.getTile().getBoard().isChecked(this.getColor(), this.getTile().getBoard().getKing(this.getColor()).getTile().getPosition())){
            System.out.println("Cannot move because CHECKED!");
            previousTile.setPiece(this);
            this.setTile(previousTile);
            toTile.setPiece(previousPiece);
            return true;
        }
        previousTile.setPiece(this);
        this.setTile(previousTile);
        toTile.setPiece(previousPiece);
        return false;
    }
    public int getMoveCastlePosition() {
        return moveCastlePosition;
    }

    public void setMoveCastlePosition(int moveCastlePosition) {
        this.moveCastlePosition = moveCastlePosition;
    }

    public int getMoveOffsetPosition() {
        return moveOffsetPosition;
    }

    public void setMoveOffsetPosition(int moveOffsetPosition) {
        this.moveOffsetPosition = moveOffsetPosition;
    }

    @Override
    public boolean capturePiece() {
        return false;
    }

    @Override
    public boolean thisPieceCaptured() {
        return false;
    }



    @Override
    public Tile getTile() {
        return this.tile;
    }


    @Override
    public Bitmap getBitMap() {
        if(color == Color.WHITE)
            return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.king00),
                    getTile().getBoard().getGridWidth(),
                    getTile().getBoard().getGridHeight());

        return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.king11),
                getTile().getBoard().getGridWidth(),
                getTile().getBoard().getGridHeight());
    }

    @Override
    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public void setHasMadeFirstMove(boolean hasMadeFirstMove){
        this.hasMadeFirstMove = hasMadeFirstMove;
    }

    public boolean getHasMadeFirstMove(){
        return this.hasMadeFirstMove;
    }
}
