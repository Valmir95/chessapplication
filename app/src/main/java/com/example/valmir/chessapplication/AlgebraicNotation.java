package com.example.valmir.chessapplication;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.Objects;

public class AlgebraicNotation {

    String file;
    int rank;

    public AlgebraicNotation(String file, int rank){
        this.file = file;
        this.rank = rank;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AlgebraicNotation that = (AlgebraicNotation) o;
        return rank == that.rank && this.file.equals(that.file);
    }

    //TODO: Create a proper hashcode.
    /*
    @Override
    public int hashCode() {
        return Objects.hash(file, rank);
    }*/
}
