package com.example.valmir.chessapplication;

/**
 * Created by valmir on 20/09/16.
 */



public class Position {


    private String x;
    private int y;
    public Position(String x, int y){
        this.x = x;
        this.y = y;
    }

    public String getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }

    public int getAccurateListPosition(){
        int listPosX = 0;
        int listPosY = 0;
        switch (this.getX()){
            case "A": listPosX = 0;
                break;
            case "B": listPosX = 1;
                break;
            case "C": listPosX = 2;
                break;
            case "D": listPosX = 3;
                break;
            case "E": listPosX = 4;
                break;
            case "F": listPosX = 5;
                break;
            case "G": listPosX = 6;
                break;
            case "H": listPosX = 7;
                break;
        }

        switch(this.getY()){
            case 8 : listPosY = 0;
                break;
            case 7 : listPosY = 8;
                break;
            case 6 : listPosY = 16;
                break;
            case 5 : listPosY = 24;
                break;
            case 4 : listPosY = 32;
                break;
            case 3 : listPosY = 40;
                break;
            case 2 : listPosY = 48;
                break;
            case 1 : listPosY = 56;
                break;


        }
        return listPosX + listPosY;
    }

    public AlgebraicNotation getAlgebraicNotation(){
        return new AlgebraicNotation(this.x.toLowerCase(), this.y);
    }

    public void setNewPosition(Position position){
        this.x = position.getX();
        this.y = position.getY();
    }

    public String toString(){
        return this.getX() + "" + getY();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (y != position.y) return false;
        return x.equals(position.x);

    }

    @Override
    public int hashCode() {
        int result = x.hashCode();
        result = 31 * result + y;
        return result;
    }
}
