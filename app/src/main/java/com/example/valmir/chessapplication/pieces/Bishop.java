package com.example.valmir.chessapplication.pieces;

import android.app.Activity;
import android.app.admin.SystemUpdatePolicy;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.valmir.chessapplication.Color;
import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Position;
import com.example.valmir.chessapplication.R;
import com.example.valmir.chessapplication.Tile;

import java.io.File;
import java.io.InputStream;

/**
 * Created by valmir on 20/09/16.
 */

public class Bishop implements Piece {
    private Color color;
    private Activity activity;
    private Tile tile;

    public Bishop(Color color, Activity activity, Tile tile){
        this.color = color;
        this.activity = activity;
        this.tile = tile;
        this.color.setTurn((this.color == Color.WHITE));

    }

    @Override
    public Piece getType() {
        return this;
    }

    @Override
    public boolean checkValidMove(Position pos) {
        int moveIntervalDown = 8;
        int moveIntervalUp = -8;
        int moveOffset = pos.getAccurateListPosition() - tile.getPosition().getAccurateListPosition();
        int moveInset = tile.getPosition().getAccurateListPosition() + pos.getAccurateListPosition();


        if(this.getTile().getBoard().getTileByPos(pos).getTileColor() != this.getTile().getTileColor())
            return false;

        if(this.getTile().getBoard().getTileByPos(pos).getPiece() != null){
            if(this.getTile().getBoard().getTileByPos(pos).getPiece().getColor() == this.getColor())
                return false;
        }
        for(int i = 1; i<=8; i++){
            if((moveOffset == (moveIntervalDown * i) + i)
                    || (moveOffset == (moveIntervalDown * i) - i)
                    || (moveOffset == (moveIntervalUp * i) + i )
                    || (moveOffset == (moveIntervalUp * i) - i)) {

                System.out.println("The moveoffset is: " + moveOffset);
                System.out.println("The amount of moves are: " + i);

                if( (checkPieceBlockade(moveOffset, i, pos, false))){
                    return false;
                }


                return true;
            }
        }

        return false;
    }

    public boolean checkPieceBlockade(int moveOffset, int amountOfMoves, Position toTile, boolean isPositive){
        int downWardOffset;
        int upWardOffset;
        int listPos;
        if(moveOffset % 9 == 0){
            downWardOffset = 9;
            upWardOffset = -9;
        }

        else{
            downWardOffset = 7;
            upWardOffset = -7;
        }


        for(int i = 1; i<amountOfMoves;i++){

            if(moveOffset > 0)  listPos = toTile.getAccurateListPosition() - (moveOffset-(i*downWardOffset));
            else listPos = toTile.getAccurateListPosition() - (moveOffset-(i*upWardOffset));

            Tile listPostTile = this.getTile().getBoard().getTiles().get(listPos);
            System.out.println(this.getColor());

            if(listPostTile.getPiece() != null && !(listPostTile.getPiece() == this)) {

                System.out.println("This piece is in the way: " + this.getTile().getBoard().getTiles().get(listPos).getPiece().getType()
                + " at pos: " + this.getTile().getBoard().getTiles().get(listPos).getPosition().getAccurateListPosition());
                    return true;
            }
            

        }
        return false;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public String getFENNotation() {
        if(this.getColor() == Color.WHITE){
            return "B";
        }
        return "b";
    }

    @Override
    public boolean movePiece(Tile toTile) {


        if(checkValidMove(toTile.getPosition())){
            this.getTile().getBoard().incrementHalfMoves();


            if(toTile.getPiece() != null){
                this.getTile().getBoard().resetHalfMoves();
            }

            if (checkForCheck(toTile)){
                return false;
            }
            toTile.setPiece(this);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            return true;
        }
        return false;

    }

    public boolean checkForCheck(Tile toTile){
        Tile previousTile = this.getTile();
        Piece previousPiece = toTile.getPiece();

        this.setTile(toTile);
        this.getTile().setPiece(this);
        previousTile.setPiece(null);
        System.out.println("check simulation move: " + this.getTile().getPosition().getX() + this.getTile().getPosition().getY());
        if(this.getTile().getBoard().isChecked(this.getColor(), this.getTile().getBoard().getKing(this.getColor()).getTile().getPosition())){
            System.out.println("Cannot move because CHECKED!");
            previousTile.setPiece(this);
            this.setTile(previousTile);
            toTile.setPiece(previousPiece);
            return true;
        }
        previousTile.setPiece(this);
        this.setTile(previousTile);
        toTile.setPiece(previousPiece);
        return false;
    }

    @Override
    public boolean capturePiece() {
        return false;
    }

    @Override
    public boolean thisPieceCaptured() {
        return false;
    }



    @Override
    public Tile getTile() {
        return this.tile;
    }


    @Override
    public Bitmap getBitMap() {
        if(color == Color.WHITE)
            return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.bishop00),
                    getTile().getBoard().getGridWidth(),
                    getTile().getBoard().getGridHeight());

        return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.bishop11),
                getTile().getBoard().getGridWidth(),
                getTile().getBoard().getGridHeight());
    }

    @Override
    public void setTile(Tile tile) {
        this.tile = tile;
    }


}
