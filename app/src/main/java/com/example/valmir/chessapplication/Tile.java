package com.example.valmir.chessapplication;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.valmir.chessapplication.move.Move;
import com.example.valmir.chessapplication.pieces.Pawn;

import java.util.ArrayList;

/**
 * Created by valmir on 20/09/16.
 */

public class Tile {
    private ImageButton button;
    private Position position;
    private Piece piece;
    private boolean isFocused;
    private String backgroundColor;
    private Board board;
    private boolean isTurn;
    private com.example.valmir.chessapplication.Color tileColor;


    public Tile(ImageButton button, Position position, Piece piece, String backgroundColor, Board board, com.example.valmir.chessapplication.Color tileColor){
        this.button = button;
        this.position = position;
        this.piece = piece;
        this.isFocused = false;
        this.backgroundColor = backgroundColor;
        this.board = board;
        this.isTurn = true;
        this.tileColor = tileColor;
        displayPiece();
    }

    public ImageButton getButton(){
        return this.button;
    }
    public Position getPosition(){
        return this.position;
    }
    public void setPiece (Piece piece){
        this.piece = piece;
    }
    public Piece getPiece(){
        return this.piece;
    }
    public String getBackgroundColor(){
        return this.backgroundColor;
    }
    public com.example.valmir.chessapplication.Color getTileColor(){
        return this.tileColor;
    }
    public boolean isFocused(){
        return this.isFocused;
    }


    public Board getBoard(){
        return this.board;
    }

    private void displayPiece(){
        if(this.piece != null)
            this.button.setImageBitmap(this.piece.getBitMap());
    }

    private boolean setFocused(){
        if(!this.isFocused() && !this.getBoard().checkAnyTilesFocused() && this.getPiece() != null && this.getPiece().getColor().getTurn()){
            this.isFocused = true;
            focusButton(true);
            return true;
        }
        else{
            this.isFocused = false;
            focusButton(false);
            return false;
        }
    }

    public void focusButton(boolean isFocused){
        if(isFocused){
            this.getButton().setBackgroundColor(Color.parseColor("#E91E63"));
            this.getBoard().addMarkedTile(this);
            try{
                ArrayList<Move> legalMoves = this.getBoard().getMoveGenerator().getAllLegalMovesPiece(this);
                for(Move m : legalMoves){
                    Tile tile = this.getBoard().getTileByPos(m.getTo());
                    tile.getButton().setBackgroundColor(Color.GREEN);
                    this.getBoard().addMarkedTile(tile);
                }
            }catch (Exception e){
                e.printStackTrace();
                return;
            }

            this.isFocused = true;
        }
        else{
            for(Tile t : this.getBoard().getMarkedTiles()){
                t.getButton().setBackgroundColor(Color.parseColor(t.getBackgroundColor()));
            }
            this.isFocused = false;

        }

    }
    public void removePiece(){
        this.setPiece(null);
        this.getButton().setImageBitmap(null);
    }

    public int[] getRankRangeFromListPosition(int listPosition){
        int [] range = new int[2];

        if(listPosition >= 0 && listPosition <8){
            range[0] = 0;
            range[1] = 7;
        }
        else if(listPosition >= 8 && listPosition <16){
            range[0] = 8;
            range[1] = 15;
        }
        else if(listPosition >= 16 && listPosition <24){
            range[0] = 16;
            range[1] = 23;
        }
        else if(listPosition >= 24 && listPosition <32){
            range[0] = 24;
            range[1] = 31;
        }
        else if(listPosition >= 32 && listPosition <40){
            range[0] = 32;
            range[1] = 39;
        }
        else if(listPosition >= 40 && listPosition <48){
            range[0] = 40;
            range[1] = 47;
        }
        else if(listPosition >= 48 && listPosition <56){
            range[0] = 48;
            range[1] = 55;
        }
        else if(listPosition >= 56 && listPosition <64){
            range[0] = 56;
            range[1] = 63;
        }

        return range;

    }

    private void movePieceFromTile(){

        if( !this.getBoard().getGameOver() && this.getBoard().getFocusedTile().getPiece().movePiece(getBoard().getTileByPos(this.getPosition()))){
            this.getPiece().setTile(this);
            this.getBoard().removeEnPassantePositions(this.getPiece().getColor());
            this.getBoard().setTurnsFalse(this.getPiece().getColor());
            this.getBoard().getFocusedTile().removePiece();
            Tile focusedTile = this.getBoard().getFocusedTile();
            this.getBoard().removeFocuses();


            this.getButton().setBackgroundColor(Color.CYAN);
            focusedTile.getButton().setBackgroundColor(Color.CYAN);

            if(com.example.valmir.chessapplication.Color.WHITE.getTurn()){
                this.getBoard().incrementFullMoves();
            }

        }
        else{
            this.getBoard().removeFocuses();
        }


    }


    public View.OnClickListener listener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            int [] rank = getRankRangeFromListPosition(getPosition().getAccurateListPosition());
            System.out.println("White checked: " + com.example.valmir.chessapplication.Color.WHITE.getChecked() + " - Black checked: " + com.example.valmir.chessapplication.Color.BLACK.getChecked());
            System.out.println(rank[0] + " " + rank[1]);
            System.out.println("A button was clicked with the position: " + getPosition().getX() + getPosition().getY()
            + " actual list position: " + getPosition().getAccurateListPosition() + " and tile color: " + getTileColor() + " and" +
                    "piece " + getPiece());
            if(!setFocused() && getBoard().getFocusedTile() != null){
                movePieceFromTile();
                System.out.println();
                getBoard().addFENToList(getBoard().generateFENStringCurrentPositions());
                String currentFen = getBoard().getFENList().get(getBoard().getFENList().size()-1);
                System.out.println("WTF !!!!!" + currentFen);
                String gameOverString = "";
                //Toast.makeText(getBoard().getActivity(), "Half-Moves: " + getBoard().getHalfMoves(), Toast.LENGTH_SHORT).show();

                if(com.example.valmir.chessapplication.Color.BLACK.getTurn()){
                    new ComputerMoveTask(getBoard()).execute(getBoard());
                }

                if(getBoard().isGameOver()){
                    if(getBoard().getHalfMoves() > 49){
                        gameOverString = "Game is over by draw of 50 move rule";
                    }
                    else{
                        gameOverString = "Game is over by stalemate! 1/2 - 1/2";
                    }
                    if(getBoard().isCheckMated()){
                        gameOverString = "Game is over by checkmate!";
                    }
                    Toast.makeText(getBoard().getActivity(), gameOverString, Toast.LENGTH_LONG).show();
                }


            }


        }
    };

}
