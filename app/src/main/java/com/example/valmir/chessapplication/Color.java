package com.example.valmir.chessapplication;

/**
 * Created by valmir on 20/11/2016.
 */

public enum Color {
    WHITE, BLACK;

    private boolean turn;
    private boolean checked = false;

    public boolean getTurn(){
        return this.turn;
    }
    public void setTurn(boolean turn){
        this.turn = turn;
    }
    public boolean getChecked(){
        return this.checked;
    }
    public void setChecked(boolean checked){
        this.checked = checked;
    }
}
