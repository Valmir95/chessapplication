package com.example.valmir.chessapplication.move;

import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Position;

public class Move {
    Piece piece;
    Position from;
    Position to;
    public Move(Piece piece, Position from, Position to){
        this.piece = piece;
        this.from = from;
        this.to = to;
    }

    public Piece getPiece() {
        return piece;
    }

    public Position getFrom() {
        return from;
    }

    public Position getTo() {
        return to;
    }
}
