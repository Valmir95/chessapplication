package com.example.valmir.chessapplication.pieces;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.example.valmir.chessapplication.Color;
import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Position;
import com.example.valmir.chessapplication.R;
import com.example.valmir.chessapplication.Tile;

/**
 * Created by valmir on 20/09/16.
 */

public class Pawn implements Piece {
    private Color color;
    private Activity activity;
    private Tile tile;
    private final int[] validMoves = {-16, -8, -7, -9, 7, 8, 9, 16};
    private boolean hasMadeFirstMove;
    private Position enPassantePosition;
    private Piece enPassanteTarget;



    public Pawn (Color color, Activity activity, Tile tile){
        this.color = color;
        this.activity = activity;
        this.tile = tile;
        this.color.setTurn((this.color == Color.WHITE));
        this.hasMadeFirstMove = false;
        this.enPassantePosition = null;
        this.enPassanteTarget = null;
    }

    @Override
    public Piece getType() {
        return this;
    }

    @Override
    //TODO: The reason the enPassantePosition gets set to null after it is set, is because the method which checks for "Check" on the king, runs this method and sets the value of enPassantePosition to null.
    //Possible solution would be to add a second parameter/method which did not change the state of the enPassantePosition when ran.
    public boolean checkValidMove(Position pos) {
        int[] moveIntervalRanges = this.getTile().getRankRangeFromListPosition(this.getTile().getPosition().getAccurateListPosition());
        int moveOffset = pos.getAccurateListPosition() - tile.getPosition().getAccurateListPosition();

        //Toast.makeText(this.getTile().getBoard().getActivity(), "Offset: " + moveOffset, Toast.LENGTH_LONG).show();

        if(this.getTile().getBoard().getTileByPos(pos).getPiece() != null){
            if(this.getTile().getBoard().getTileByPos(pos).getPiece().getColor() == this.getColor())
                return false;
        }
        System.out.println("Move interval ranges: " + moveIntervalRanges[0] + " " + moveIntervalRanges[1]);
        if(moveOffset == validMoves[0] || moveOffset == validMoves[7]){
            if((this.getTile().getBoard().getTiles().get(this.getTile().getPosition().getAccurateListPosition()+validMoves[1]).getPiece() != null)
                    && this.getColor() == Color.WHITE
                    || (this.getTile().getBoard().getTiles().get(this.getTile().getPosition().getAccurateListPosition()+validMoves[5]).getPiece() != null)
                    && this.getColor() == Color.BLACK){
                return false;
            }
        }




        //White pawn move
        if(this.getColor() == Color.WHITE){
            if(!this.getMadeFirstMove()){

                if(((moveOffset == validMoves[1]  || moveOffset == validMoves[0])
                        && (getTile().getBoard().getTileByPos(pos).getPiece() == null)
                        || ((moveOffset == validMoves[2]
                        || moveOffset == validMoves[3])
                        && (pos.getAccurateListPosition() < moveIntervalRanges[0]) && (this.getTile().getBoard().getTileByPos(pos).getPiece() != null)))){


                    if( (moveOffset == validMoves[2] || moveOffset == validMoves[3]) && this.getTile().getTileColor() != this.getTile().getBoard().getTileByPos(pos).getTileColor()) return false;

                    if(moveOffset == validMoves[0]){
                        this.enPassantePosition = new Position(this.getTile().getPosition().getX(), this.getTile().getPosition().getY() + 1);
                    }
                    else{
                        this.enPassantePosition = null;
                    }
                    return true;
                }
                else{
                    return false;
                }

            }
            else{

                Piece enPassantePieceOne = this.getTile().getBoard().getTiles().get(this.getTile().getPosition().getAccurateListPosition()+1).getPiece();
                Piece enPassantePieceTwo = this.getTile().getBoard().getTiles().get(this.getTile().getPosition().getAccurateListPosition()-1).getPiece();
                Piece enPassantePiece = null;
                if(enPassantePieceOne != null || enPassantePieceTwo != null){
                    if(enPassantePieceOne != null && enPassantePieceOne instanceof Pawn && ((Pawn) enPassantePieceOne).getEnPassantePosition() != null){
                        enPassantePiece = enPassantePieceOne;
                    }
                    else if(enPassantePieceTwo != null && enPassantePieceTwo instanceof Pawn && ((Pawn) enPassantePieceTwo).getEnPassantePosition() != null){
                        enPassantePiece = enPassantePieceTwo;
                    }
                }

                if(!(this.getTile().getBoard().getTileByPos(pos).getPiece() instanceof King)){
                    this.enPassantePosition = null;
                }
                if((moveOffset == validMoves[1]
                        && getTile().getBoard().getTileByPos(pos).getPiece() == null)
                        || ((moveOffset == validMoves[2]
                        || moveOffset == validMoves[3])
                        && (pos.getAccurateListPosition() < moveIntervalRanges[0]) && (this.getTile().getBoard().getTileByPos(pos).getPiece() != null || (enPassantePiece != null && enPassantePiece instanceof Pawn && ((Pawn) enPassantePiece).getEnPassantePosition().toString().equals(pos.toString()))))){


                    if((enPassantePiece != null && enPassantePiece instanceof Pawn && ((Pawn) enPassantePiece).getEnPassantePosition().toString().equals(pos.toString()))){
                        this.enPassanteTarget = enPassantePiece;
                    }
                    return true;
                }
                return false;
            }

        }
        //Black pawn move
        else{
            if(!this.getMadeFirstMove()){
                if(((moveOffset == validMoves[5] || moveOffset == validMoves[7])
                        && (getTile().getBoard().getTileByPos(pos).getPiece() == null)
                        || ( (moveOffset == validMoves[4]
                        || moveOffset == validMoves[6])
                        && (pos.getAccurateListPosition() > moveIntervalRanges[1]) && (this.getTile().getBoard().getTileByPos(pos).getPiece() != null)))){

                    if( (moveOffset == validMoves[4] || moveOffset == validMoves[6]) && this.getTile().getTileColor() != this.getTile().getBoard().getTileByPos(pos).getTileColor()) return false;

                    if(moveOffset == validMoves[7]){
                        this.enPassantePosition = new Position(this.getTile().getPosition().getX(), this.getTile().getPosition().getY() - 1);
                    }
                    else{
                        this.enPassantePosition = null;
                    }
                    return true;
                }
                else{
                    return false;
                }



            }
            else{
                Piece enPassantePieceOne = this.getTile().getBoard().getTiles().get(this.getTile().getPosition().getAccurateListPosition()+1).getPiece();
                Piece enPassantePieceTwo = this.getTile().getBoard().getTiles().get(this.getTile().getPosition().getAccurateListPosition()-1).getPiece();
                Piece enPassantePiece = null;
                if(enPassantePieceOne != null || enPassantePieceTwo != null){
                    if(enPassantePieceOne != null && enPassantePieceOne instanceof Pawn &&((Pawn) enPassantePieceOne).getEnPassantePosition() != null){
                        enPassantePiece = enPassantePieceOne;
                    }
                    else if(enPassantePieceTwo != null && enPassantePieceTwo instanceof Pawn && ((Pawn) enPassantePieceTwo).getEnPassantePosition() != null){
                        enPassantePiece = enPassantePieceTwo;
                    }
                }

                if(!(this.getTile().getBoard().getTileByPos(pos).getPiece() instanceof King)){
                    this.enPassantePosition = null;
                }
                if((moveOffset == 8
                        && getTile().getBoard().getTileByPos(pos).getPiece() == null)
                        || ((moveOffset == validMoves[4]
                        || moveOffset == validMoves[6])
                        && (pos.getAccurateListPosition() > moveIntervalRanges[1]) && (this.getTile().getBoard().getTileByPos(pos).getPiece() != null || (enPassantePiece != null && enPassantePiece instanceof Pawn && ((Pawn) enPassantePiece).getEnPassantePosition().toString().equals(pos.toString()))))){


                    if((enPassantePiece != null && enPassantePiece instanceof Pawn && ((Pawn) enPassantePiece).getEnPassantePosition().toString().equals(pos.toString()))){
                        this.enPassanteTarget = enPassantePiece;
                    }
                    return true;
                }
                return false;

            }

        }

    }
    @Override
    public Color getColor() {
        return this.color;
    }

    private boolean getMadeFirstMove(){
        return this.hasMadeFirstMove;
    }

    private void setMadeFirstMove(boolean hasMadeFirstMove){
        this.hasMadeFirstMove = hasMadeFirstMove;
    }

    @Override
    public String getFENNotation() {
        if(this.getColor() == Color.WHITE){
            return "P";
        }
        return "p";
    }

    @Override
    public boolean movePiece(Tile toTile) {
        if(checkValidMove(toTile.getPosition())){
            this.getTile().getBoard().incrementHalfMoves();

            this.getTile().getBoard().resetHalfMoves();
            if(checkForCheck(toTile)){
                return false;
            }

            if(pawnPromotion(toTile)){
                return true;
            }

            toTile.setPiece(this);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            if(this.enPassanteTarget != null){
                removeEnpassantTarget(this.enPassanteTarget);
            }
            if(!this.getMadeFirstMove()) this.setMadeFirstMove(true);
            return true;
        }

        return false;

    }


    private void removeEnpassantTarget(Piece enPassanteTarget){
        enPassanteTarget.getTile().setPiece(null);
        enPassanteTarget.getTile().getButton().setImageBitmap(null);
        this.enPassanteTarget = null;
    }

    public boolean checkForCheck(Tile toTile){
        Tile previousTile = this.getTile();
        Piece previousPiece = toTile.getPiece();

        this.setTile(toTile);
        this.getTile().setPiece(this);
        previousTile.setPiece(null);
        System.out.println("check simulation move: " + this.getTile().getPosition().getX() + this.getTile().getPosition().getY());
        if(this.getTile().getBoard().isChecked(this.getColor(), this.getTile().getBoard().getKing(this.getColor()).getTile().getPosition())){
            System.out.println("Cannot move because CHECKED!");
            previousTile.setPiece(this);
            this.setTile(previousTile);
            toTile.setPiece(previousPiece);
            return true;
        }
        previousTile.setPiece(this);
        this.setTile(previousTile);
        toTile.setPiece(previousPiece);
        return false;
    }


    private boolean pawnPromotion(Tile toTile){
        int[] moveIntervalRangesWhitePromotion = this.getTile().getRankRangeFromListPosition(0);
        int[] moveIntervalRangesBlackPromotion = this.getTile().getRankRangeFromListPosition(56);
        Activity activity = this.getTile().getBoard().getActivity();
        Tile fromTile = this.getTile();

        if(this.getColor() == Color.WHITE && (toTile.getPosition().getAccurateListPosition() >= moveIntervalRangesWhitePromotion[0]
        && toTile.getPosition().getAccurateListPosition() <= moveIntervalRangesWhitePromotion[1]) && checkValidMove(toTile.getPosition())){
            Queen pawnPromotion = new Queen(Color.WHITE, activity, toTile);
            toTile.setPiece(pawnPromotion);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            toTile.getPiece().setTile(toTile);
            fromTile.setPiece(null);
            fromTile.getButton().setImageBitmap(null);
            System.out.println(moveIntervalRangesWhitePromotion[0] + " " + moveIntervalRangesWhitePromotion[1] + toTile.getPiece().getBitMap());
            return true;
        }
        else if(this.getColor() == Color.BLACK && (toTile.getPosition().getAccurateListPosition() >= moveIntervalRangesBlackPromotion[0]
                && toTile.getPosition().getAccurateListPosition() <= moveIntervalRangesBlackPromotion[1])){
            Queen pawnPromotion = new Queen(Color.BLACK, activity, toTile);
            toTile.setPiece(pawnPromotion);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            toTile.getPiece().setTile(toTile);
            fromTile.setPiece(null);
            fromTile.getButton().setImageBitmap(null);
            System.out.println(moveIntervalRangesBlackPromotion[0] + " " + moveIntervalRangesBlackPromotion[1] + toTile.getPiece().getBitMap());
            return true;
        }

        return false;
    }

    @Override
    public boolean capturePiece() {
        return false;
    }

    public Position getEnPassantePosition(){
        return this.enPassantePosition;
    }

    public void setEnPassantePosition(Position position){
        this.enPassantePosition = position;
    }

    @Override
    public boolean thisPieceCaptured() {
        return false;
    }

    @Override
    public Tile getTile(){
        return this.tile;
    }

    @Override
    public Bitmap getBitMap() {
        if(color == Color.WHITE)
            return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.pawn00),
                    getTile().getBoard().getGridWidth(),
                    getTile().getBoard().getGridHeight());

        return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.pawn11),
                getTile().getBoard().getGridWidth(),
                getTile().getBoard().getGridHeight());
    }
    @Override
    public void setTile(Tile tile) {
        this.tile = tile;
    }
}
