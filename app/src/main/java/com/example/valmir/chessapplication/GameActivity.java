package com.example.valmir.chessapplication;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.valmir.chessapplication.stockfish.Stockfish;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.json.JSONObject;
import org.w3c.dom.Text;

import cn.iwgang.countdownview.CountdownView;


public class GameActivity extends AppCompatActivity {
    private Button btnRestart;
    private Intent intent;

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private Board board;
    private CountdownView[] countDownTimers;
    private CountdownView whiteTextTimer;
    private CountdownView blackTextTimer;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        countDownTimers = new CountdownView[2];
        whiteTextTimer = (CountdownView) findViewById(R.id.whiteTimer);
        blackTextTimer = (CountdownView) findViewById(R.id.blackTimer);


        GridLayout gridLayout = (GridLayout) findViewById(R.id.gridLayout);
        btnRestart = (Button)findViewById(R.id.restartButton);

        board = new Board(gridLayout, this, getScreenResolution(), whiteTextTimer, blackTextTimer);

        Toast.makeText(this, "" + getScreenResolution().widthPixels
                + " " + getScreenResolution().heightPixels, Toast.LENGTH_LONG).show();


        board.drawBoard(120*1000, 120*1000);
    }



    public CountdownView getWhiteTextTimer(){
        return this.whiteTextTimer;
    }

    public CountdownView getBlackTextTimer(){
        return this.blackTextTimer;
    }


    /*private void interstitialInstantiating(){
        mAdView = (AdView) findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstatial_ad));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //showInterstitial();
            }
        });

    }*/

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }



    /*@Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }*/

    public void restartGame(View v){
        Tile e2 = board.getTileByAn(new AlgebraicNotation("e", 2));
        Tile e3 = board.getTileByAn(new AlgebraicNotation("e", 4));

        board.move(e2, e3);
    }

    private DisplayMetrics getScreenResolution(){
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        return metrics;
    }
}
