package com.example.valmir.chessapplication.pieces;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.example.valmir.chessapplication.Color;
import com.example.valmir.chessapplication.Piece;
import com.example.valmir.chessapplication.Position;
import com.example.valmir.chessapplication.R;
import com.example.valmir.chessapplication.Tile;

/**
 * Created by valmir on 20/09/16.
 */

public class Knight implements Piece {
    private Color color;
    private Activity activity;
    private Tile tile;
    private final int[] validMoves = {-17, -15, -10, -6, 17, 15, 10, 6};

    public Knight(Color color, Activity activity, Tile tile){
        this.color = color;
        this.activity = activity;
        this.tile = tile;
        this.color.setTurn((this.color == Color.WHITE));

    }
    @Override
    public Piece getType() {
        return this;
    }

    @Override
    public boolean checkValidMove(Position pos) {
        int moveOffset = pos.getAccurateListPosition() - tile.getPosition().getAccurateListPosition();
        //Toast.makeText(this.tile.getBoard().getActivity(), "Move offset: " + moveOffset, Toast.LENGTH_LONG).show();

        if(this.getTile().getBoard().getTileByPos(pos).getTileColor() == this.getTile().getTileColor()){
            return false;
        }

        if(this.getTile().getBoard().getTileByPos(pos).getPiece() != null){
            if(this.getTile().getBoard().getTileByPos(pos).getPiece().getColor() == this.getColor())
                return false;
        }
        for(int i = 0; i<validMoves.length; i++){
            if(moveOffset == validMoves[i])
                return true;
        }
        return false;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public boolean movePiece(Tile toTile) {

        if(checkValidMove(toTile.getPosition())){
            this.getTile().getBoard().incrementHalfMoves();

            if(toTile.getPiece() != null){
                this.getTile().getBoard().resetHalfMoves();
            }
            if(checkForCheck(toTile)){
                return false;
            }
            toTile.setPiece(this);
            toTile.getButton().setImageBitmap(toTile.getPiece().getBitMap());
            return true;
        }
        return false;

    }

    @Override
    public String getFENNotation() {
        if(this.getColor() == Color.WHITE){
            return "N";
        }
        return "n";
    }

    public boolean checkForCheck(Tile toTile){
        Tile previousTile = this.getTile();
        Piece previousPiece = toTile.getPiece();

        this.setTile(toTile);
        this.getTile().setPiece(this);
        previousTile.setPiece(null);
        System.out.println("check simulation move: " + this.getTile().getPosition().getX() + this.getTile().getPosition().getY());
        if(this.getTile().getBoard().isChecked(this.getColor(), this.getTile().getBoard().getKing(this.getColor()).getTile().getPosition())){
            System.out.println("Cannot move because CHECKED!");
            previousTile.setPiece(this);
            this.setTile(previousTile);
            toTile.setPiece(previousPiece);
            return true;
        }
        previousTile.setPiece(this);
        this.setTile(previousTile);
        toTile.setPiece(previousPiece);
        return false;
    }


    @Override
    public boolean capturePiece() {
        return false;
    }

    @Override
    public boolean thisPieceCaptured() {
        return false;
    }


    @Override
    public Tile getTile() {
        return this.tile;
    }


    @Override
    public Bitmap getBitMap() {
        if(color == Color.WHITE)
            return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.knight00),
                    getTile().getBoard().getGridWidth(),
                    getTile().getBoard().getGridHeight());

        return this.getTile().getBoard().scaleBitmap(BitmapFactory.decodeResource(activity.getResources(),
                R.drawable.knight11),
                getTile().getBoard().getGridWidth(),
                getTile().getBoard().getGridHeight());
    }

    @Override
    public void setTile(Tile tile) {
        this.tile = tile;
    }
}
