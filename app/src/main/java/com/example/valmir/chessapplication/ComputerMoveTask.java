package com.example.valmir.chessapplication;

import android.os.AsyncTask;

public class ComputerMoveTask extends AsyncTask<Board, Void, String[]> {

    private Board board;

    public ComputerMoveTask(Board board){
        this.board = board;
    }

    protected String[] doInBackground(Board... boards) {
        int listSize = this.board.getFENList().size();
        String latestFEN = this.board.getFENList().get(listSize-1);
        String[] bestMoveAndVal = new String[2];
        try{
            bestMoveAndVal = this.board.getStockfish().getBestMoveAndEval(latestFEN, 0.2);
        }catch (Exception e){
            e.printStackTrace();
        }
        return bestMoveAndVal;
    }


    protected void onPostExecute(String[] bestMoveAndVal) {
        if(bestMoveAndVal[0] == null && bestMoveAndVal[1] == null) return;
        String fromAnPos = bestMoveAndVal[0].substring(0,2);
        String toAnPos = bestMoveAndVal[0].substring(bestMoveAndVal[0].length()-2);
        Tile from = this.board.getTileByAn(new AlgebraicNotation(fromAnPos.substring(0,1), Integer.parseInt(fromAnPos.substring(fromAnPos.length()-1))));
        Tile to = this.board.getTileByAn(new AlgebraicNotation(toAnPos.substring(0,1), Integer.parseInt(toAnPos.substring(toAnPos.length()-1))));
        this.board.move(from ,to);
    }

}
