package com.example.valmir.chessapplication.stockfish;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * A simple and efficient client to run Stockfish from Java
 *
 * @author Rahul A R
 *
 */
public class Stockfish {

    public Stockfish(){

    }


    public String[] getBestMoveAndEval(String fen, double secondsToThink) throws Exception {
        System.out.println("FEN STRING!!!! " + fen);
        final String POST_PARAMS = "{\n" + "\"fen\": \"" + fen + "\",\r\n" +
                "    \"secondsToThink\": " + secondsToThink + "\n}";
        System.out.println(POST_PARAMS);
        URL obj = new URL("http://a32fdfa0.ngrok.io/api/stockfish/best_move");
        HttpURLConnection postConnection = (HttpURLConnection) obj.openConnection();
        postConnection.setRequestMethod("POST");
        postConnection.setRequestProperty("Content-Type", "application/json");
        postConnection.setDoOutput(true);
        OutputStream os = postConnection.getOutputStream();
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        int responseCode = postConnection.getResponseCode();
        System.out.println("POST Response Code :  " + responseCode);
        System.out.println("POST Response Message : " + postConnection.getResponseMessage());
        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    postConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();
            // print result
            JSONObject jsonObject = new JSONObject(response.toString());
            String bestMove = jsonObject.get("bestmove").toString();
            String eval = jsonObject.get("eval").toString();
            return new String[]{bestMove, eval};
        } else {
            throw new Exception("Could not contact stockfish cloud");
        }
    }
}